import {Component, ViewEncapsulation} from "@angular/core";
import DetailKurzLektor from "../model/DetailKurzLektor";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['../../public/styles/styles.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {

    private detailkurzlektor:DetailKurzLektor = {
        name: 'ITA-04 - Angular 2',
        description: 'Angular 2 workshop description',
        language: 'Angular 2+',
        startDate: new Date(),
        lessons: 10
    };


    constructor() {
        console.log(this.detailkurzlektor);
    }

    private languageChange(event) {
        console.log('lang change', event);
    }
}
