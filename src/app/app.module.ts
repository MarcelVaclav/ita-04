import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

/*third imports parties **/

import { BsDropdownModule } from 'ngx-bootstrap';
import {DemoDropdownBasicComponent} from "../dropDownMenu/dropDownMenu.component";
import {DemoDropdownBasicBellComponent} from "../dropDownMenuBell/dropDownMenuBell.component";
import {DemoDropdownBasicFlagComponent} from "../dropDownMenuFlag/dropdownMenuFlag.component";


import {AppComponent} from "./app.component";
import {NavbarComponent} from "../navbar/navbar.component";
import {SidebarComponent} from "../sidebar/sidebar.component";
import {ContentComponent} from "../content/content.component";
import {FooterComponent} from "../footer/footer.component";
import {DetailKurzLektorComponent} from "../detailkurzlektor/detailkurzlektor.component";
import {DetailKurzuStudentaComponent} from "../detailkurzstudent/detailkurzustudenta.component";
import {LoginScreenComponent} from "../loginscreen/loginscreen.component";
import {PridatKurzComponent} from "../pridatkurz/pridatkurz.component";
import {PridatLektoraComponent} from "../pridatlektora/pridatlektora.component";
import {PridatStudentaComponent} from "../pridatstudenta/pridatstudenta.component";
import {ProfilComponent} from "../profil/profil.component";


@NgModule({
    imports: [
        BrowserModule,
        BsDropdownModule.forRoot()
    ],
    declarations: [
        AppComponent,
        NavbarComponent,
        SidebarComponent,
        ContentComponent,
        FooterComponent,
        DetailKurzLektorComponent,
        DetailKurzuStudentaComponent,
        LoginScreenComponent,
        PridatKurzComponent,
        PridatLektoraComponent,
        PridatStudentaComponent,
        ProfilComponent,
        DemoDropdownBasicComponent,
        DemoDropdownBasicBellComponent,
        DemoDropdownBasicFlagComponent
    ],
    providers: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
