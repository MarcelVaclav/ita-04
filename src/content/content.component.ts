import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import DetailKurzLektor from "../model/DetailKurzLektor";

@Component({
    selector: 'content-component',
    templateUrl: './content.component.html'
})
export class ContentComponent implements OnInit {

    @Input()
    private detailkurzlektor: DetailKurzLektor;

    @Output()
    private language:EventEmitter<string> = new EventEmitter();

    constructor() {
        console.log(this.detailkurzlektor);
    }

    ngOnInit(): void {
        console.log(this.detailkurzlektor);
    }

    private changeLanguage() {
        console.log('change to React');

        this.language.emit('React');
    }
}