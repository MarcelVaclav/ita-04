export default class DetailKurzLektor {
    public name: String;
    public description: String;
    public language: String;
    public startDate: Date;
    public lessons: number;

    constructor(private name: String, private description: String ) {
        this.name = name;
        this.description = description;

    }
};