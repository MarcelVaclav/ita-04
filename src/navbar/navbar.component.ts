import {Component} from "@angular/core";

@Component({
    selector: 'navbar-component',
    templateUrl: './navbar.component.html'
})
export class NavbarComponent {

    private user;

    constructor() {
        this.user = {
            name: 'Marcel',
            surname: 'Václav'
        };
    }

    private onEnvelopClick() {
        console.log('click');
    }

}

@Component({
    selector: 'demo-dropdown-basic',
    templateUrl: './navbar.component.html'
})
export class DemoDropdownBasicComponent {

}