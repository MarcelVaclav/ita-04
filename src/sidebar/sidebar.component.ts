import {Component} from "@angular/core";

@Component({
    selector: 'sidebar-component',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent {

    private user;

    constructor() {
        this.user = {
            name: 'Marcel',
            surname: 'Václav'
        };
    }
}